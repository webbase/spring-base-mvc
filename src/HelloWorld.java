
public class HelloWorld {
	 private String message;

	   public void setMessage(String message){
	      this.message  = message;
	   }
	   public void getMessage(){
	      System.out.println("Your Message : " + message);
	   }
	   static int factorial(int n){      
	          if (n == 1)      
	            return 1;      
	          else      
	            return(n * factorial(n-1));      
	    }    
	   public static void main(String[] args) {
		   System.out.println("Factorial of 5 is: "+factorial(5));  
			System.out.println("Done!!");
	}
}
